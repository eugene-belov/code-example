## code-example

```javascript 
    this.mapWaypoints = [];
    const transportSearchFields: TransportPageSearchDTO = {...this.searchCriteria, pageInfo: {...this.paginationPageInfo}};
    const tourSearchFields: TourSearchDTO = <TourSearchDTO>{ operationDateStart: this.searchCriteria.operationDateStart };
    const loader$ = this.translateService.get('general.loading');
    const transports$ = this.transportService.findAllTransports(transportSearchFields);
    const tours$ = this.toursService.findToursWaypoints(tourSearchFields);
    const waypoints$ = this.mapService.ejectWaypoints$;

    const join$ = forkJoin(
      transports$.pipe(
        tap((page: PageDTO<TransportDTO>) => {
          this.transportsPage = page;
          this.filteredOnlyCreatedTransports = page.content.filter(transport => transport.status === Statuses.CREATED);
        })
      ),
      tours$.pipe(waypoints$)
    );

    loader$.pipe(
      tap((message) => this.loader.show(message)),
      switchMap(() => join$.pipe(
          tap(() => this.loader.hide()),
          tap(([transports, waypoints]) => {
            this.mapWaypoints = [...waypoints];
          })
        )
      )
    ).subscribe();
```